package task2;

import java.util.Scanner;

@FunctionalInterface
interface CommandInterface {
    void command(String name, String text);
}

public class Command {
    private String name;

    public void whispers(String text) {
        CommandInterface cmd = (a, b) -> {
            System.out.println(a + " whispers: " + b);
        };
        cmd.command(name, text);
    }


    public void says(String text) {
        CommandInterface cmdif = (a, b) -> System.out.println(a + " says: " + b);
        cmdif.command(name, text);
    }


    public void saysLoud(String text) {
        CommandInterface cmd = new CommandInterface() {
            @Override
            public void command(String name, String text) {
                System.out.println(name + " says loud: " + text);
            }

        };
        cmd.command(name, text);
    }

    public void screams(String text) {
        System.out.println(name + " screams: " + text + "!!!");
    }

    public String ask() {
        System.out.println("Input the text here please");
        Scanner sc = new Scanner(System.in);
        String text = sc.next();
        return text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
